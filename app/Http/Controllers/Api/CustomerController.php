<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use Illuminate\Http\Request;
use App\Models\Discount;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CustomerController extends Controller
{
    public function signup(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:50',
            'email' => 'required|email|unique:customers',
            'phone' => 'required|unique:customers'
        ],[
            'email.unique' => 'Email already registered try with new email',
            'phone.unique' => 'Phone already registered try with new phone'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'result' => 0,
                'message' => $validator->messages()->first()
            ]);
        }
        DB::beginTransaction();
        $received_discount = NULL;
        try{
            $discounts = Discount::where('status',1)->get();
            if($discounts->count() > 0){
                $discount = self::get_random_discount($discounts);
                $received_discount = Discount::find($discount->id);
                if(!empty($received_discount)){
                    Customer::create([
                        'name' => $request->name,
                        'email' => $request->email,
                        'phone' => $request->phone,
                        'discount_id' => $received_discount->id
                    ]);
                    $received_discount->used_discount_count += 1;
                    if($received_discount->total_discount_count == $received_discount->used_discount_count){
                        $received_discount->status = 2;
                    }
                    $received_discount->save();
                }else{
                    return response()->json([
                        'result' => 0,
                        'message' => 'No more discount available.'
                    ]);
                }
            }else{
                return response()->json([
                    'result' => 0,
                    'message' => 'No more discount available.'
                ]);
            }
            DB::commit();
            return response()->json([
                'result' => 1,
                'message' => 'You received '.$received_discount->discount_value.' Rs. Discount'
            ]);
        }catch (\Exception $e){
            DB::rollBack();
            return response()->json([
                'result' => 0,
                'message' => $e->getMessage()
            ]);
        }
    }

    protected static function get_random_discount($discounts){
        $result = array();
        $products = [];
        foreach ($discounts as $key => $val) {
            $products[$key] = $val['discount_probability'];
        }
        $prob_sum = array_sum($products);
        asort($products);
        foreach ($products as $k => $product) {
            $random_number = mt_rand(1, $prob_sum);
            if ($random_number <= $product) {
                $result = $discounts[$k];
                break;
            } else {
                $prob_sum -= $product;
            }
        }
        return $result;
    }
}
