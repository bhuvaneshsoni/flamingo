<?php

use Illuminate\Database\Seeder;

class DiscountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $discount_array = array(
            array('discount_value' => 50, 'total_discount_count' => 15, 'discount_probability' => 8),
            array('discount_value' => 100, 'total_discount_count' => 12, 'discount_probability' => 7),
            array('discount_value' => 200, 'total_discount_count' => 10, 'discount_probability' => 6),
            array('discount_value' => 500, 'total_discount_count' => 8, 'discount_probability' => 5),
            array('discount_value' => 1000, 'total_discount_count' => 5, 'discount_probability' => 4),
            array('discount_value' => 2000, 'total_discount_count' => 4, 'discount_probability' => 3),
            array('discount_value' => 5000, 'total_discount_count' => 2, 'discount_probability' => 2),
            array('discount_value' => 10000, 'total_discount_count' => 1, 'discount_probability' => 1),
        );
        foreach ($discount_array as $discount){
            \App\Models\Discount::create($discount);
        }
    }
}
